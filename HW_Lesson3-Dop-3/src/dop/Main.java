package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Есть круг с центром в начале координат и радиусом 4");
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите координату точки 'x': ");
		double x = sc.nextDouble();
		System.out.println("Введите координату точки 'y': ");
		double y = sc.nextDouble();

		System.out.println(Math.pow(x,2) + Math.pow(y,2));
		
		if (Math.pow(x,2) + Math.pow(y,2) <= 16) {
			System.out.println("Введенная точка находится внутри круга");
		}else {
			System.out.println("Введенная точка вне круга");
		}
	}
}
