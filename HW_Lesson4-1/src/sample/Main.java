package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Введите целое число от 5 до 15: ");
		Scanner sc = new Scanner(System.in);
		long n = sc.nextLong();
		long mult = 1;
		if ((4 < n) && (n < 16)) {
			for (int i = 1; i <= n; i++) {
				mult = mult * i;
			}
			System.out.println("Факториал числа " + n + "!" + " = " + mult);
		}else {
			System.out.println("Число за рамками заданного диапазона");
		}
	}
}
