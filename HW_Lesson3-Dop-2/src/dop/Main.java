package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите шестизначное число: ");
		int num = sc.nextInt();

		int numFirst = num / 100000;
		int numSecond = num / 10000 % 10;
		int numThird = num / 1000  % 10;
		int numFourth = num / 100 % 10;
		int numFifth = num / 10  % 10;
		int numSixth = num % 10;
		
		if ((numFirst == numSixth) && (numSecond == numFifth) && (numThird == numFourth)) {
			System.out.println("Введённое число - палиндром!");
		}else {
			System.out.println("Введённое число палиндромом не является.");
		}
	}
}
