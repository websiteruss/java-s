package sample;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.println("Введите первое число: ");
    int num1 = sc.nextInt();
    System.out.println("Введите второе число: ");
    int num2 = sc.nextInt();
    System.out.println("Введите третье число: ");
    int num3 = sc.nextInt();
    System.out.println("Введите четвёртое число: ");
    int num4 = sc.nextInt();

    int maxNum = num1;
    if (num2 > maxNum) {
    	maxNum = num2;
    }
    if (num3 > maxNum) {
    	maxNum = num3;
    }
    if (num4 > maxNum) {
    	maxNum = num4;
    }
    System.out.println("Максимальное число: " + maxNum);
  }
}