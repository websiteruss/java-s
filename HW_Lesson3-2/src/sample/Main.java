package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    System.out.println("Введите номер квартиры: ");
	    
	    int flatNum = sc.nextInt();
	    int entrance = (flatNum / 36) + 1;
	    int floor = (flatNum / 4) + 1;
	    
	    if ((flatNum <= 0) || (flatNum > 180)) {
	    	System.out.println("Такой квартиры нет в этом доме");
	    }else if ((flatNum % 36 == 0) && (flatNum % 4 == 0)) {
	    	System.out.println("Подъезд номер: " + (entrance - 1) + " , " + "Этаж: " + (flatNum / 4) / (entrance - 1));
	    }else if ((flatNum % 36 != 0) && (flatNum % 4 != 0)) {
	    	System.out.println("Подъезд номер: " + entrance + " , " + "Этаж: " + (floor - (9 * (entrance - 1))));    
	    }else {
	    	System.out.println("Подъезд номер: " + entrance + " , " + "Этаж: " + ((floor - 1) - (9 * (entrance - 1))));
	    }
	}
}
