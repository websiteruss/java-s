package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите четырёхзначный номер билета: ");
		int num = sc.nextInt();

		int numFirst = num / 1000;
		int numSecond = num / 100 % 10;
		int numThird = num / 10  % 10;
		int numFourth = num % 10;
		
		if ((numFirst + numSecond) == (numThird + numFourth)) {
			System.out.println("Поздравляем, билет счастливый!");
		}else {
			System.out.println(" :( ");
		}
	}
}
